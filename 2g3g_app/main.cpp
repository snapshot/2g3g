#include <QCoreApplication>
#include <QTimer>
#include <QStringList>
#include "switcher.h"
#include "iconswitcher.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    Switcher switcher;

    QStringList arguments = app.arguments();
    if (arguments.contains("-f")) {
        IconSwitcher iconswitcher;
        iconswitcher.createIcon();
        return 0;
    }

    QTimer::singleShot(0, &switcher, SLOT(changeMode()));

    app.exec();
}
