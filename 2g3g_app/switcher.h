#ifndef SWITCHER_H
#define SWITCHER_H

#include <QObject>
#include <RadioAccess>

class Switcher : public QObject
{
    Q_OBJECT

public:
    explicit Switcher(QObject *parent = 0);
    
public slots:
    void changeMode();

private slots:
    void modeChanged(int mode);
    void modeChangeFailed(QString error);

private:
    Cellular::RadioAccess radio;
    Cellular::RadioAccess::Mode mode1;
    Cellular::RadioAccess::Mode mode2;
    Cellular::RadioAccess::Mode mode3;
    bool dynIcon;
    bool notifications;
};

#endif // SWITCHER_H
