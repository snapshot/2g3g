CONFIG += cellular-qt meegotouchcore link_pkgconfig
PKGCONFIG += gq-gconf

SOURCES += main.cpp \
    switcher.cpp \
    iconswitcher.cpp
HEADERS += \
    switcher.h \
    iconswitcher.h

include(../common/common.pri)

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_harmattan/postrm
