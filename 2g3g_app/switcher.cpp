#include "switcher.h"
#include "iconswitcher.h"
#include <QCoreApplication>
#include <MNotification>
#include <gconfitem.h>

//#define GCONF_DISABLED 0
//#define GCONF_2G   1
//#define GCONF_3G   2
//#define GCONF_DUAL 3

Switcher::Switcher(QObject *parent) :
    QObject(parent)
{
    GConfItem gconfMode1("/apps/2g3g/mode1");
    GConfItem gconfMode2("/apps/2g3g/mode2");
    GConfItem gconfMode3("/apps/2g3g/mode3");
    GConfItem gconfDynIcon("/apps/2g3g/dynicon");
    GConfItem gconfNotifications("/apps/2g3g/notifications");

    if(!gconfDynIcon.value().isValid())
        gconfDynIcon.set(QVariant(true));
    if(!gconfNotifications.value().isValid())
        gconfNotifications.set(QVariant(true));

    if(!gconfMode1.value().isValid())
        gconfMode1.set(QVariant(GCONF_2G));
    if(!gconfMode2.value().isValid())
        gconfMode2.set(QVariant(GCONF_DUAL));
    if(!gconfMode3.value().isValid())
        gconfMode3.set(QVariant(GCONF_DISABLED));

    dynIcon = gconfDynIcon.value().toBool();
    notifications = gconfNotifications.value().toBool();

    QString stringMode1 = gconfMode1.value().toString();
    QString stringMode2 = gconfMode2.value().toString();
    QString stringMode3 = gconfMode3.value().toString();

    int valueMode1 = gconfMode1.value().toInt();
    int valueMode2 = gconfMode2.value().toInt();
    int valueMode3 = gconfMode3.value().toInt();

    switch (valueMode1) {
    case GCONF_2G:
        mode1 = Cellular::RadioAccess::OnlyTwoG;
        break;
    case GCONF_3G:
        mode1 = Cellular::RadioAccess::OnlyThreeG;
        break;
    case GCONF_DUAL:
        mode1 = Cellular::RadioAccess::DualMode;
        break;
    default:
        mode1 = Cellular::RadioAccess::OnlyTwoG;
        gconfMode1.set(QVariant(GCONF_2G));
    }

    switch (valueMode2) {
    case GCONF_2G:
        mode2 = Cellular::RadioAccess::OnlyTwoG;
        break;
    case GCONF_3G:
        mode2 = Cellular::RadioAccess::OnlyThreeG;
        break;
    case GCONF_DUAL:
        mode2 = Cellular::RadioAccess::DualMode;
        break;
    default:
        mode2 = Cellular::RadioAccess::OnlyTwoG;
        gconfMode2.set(QVariant(GCONF_DUAL));
    }


    switch (valueMode3) {
    case GCONF_2G:
        mode3 = Cellular::RadioAccess::OnlyTwoG;
        break;
    case GCONF_3G:
        mode3 = Cellular::RadioAccess::OnlyThreeG;
        break;
    case GCONF_DUAL:
        mode3 = Cellular::RadioAccess::DualMode;
        break;
    default:
        mode3 = Cellular::RadioAccess::Unknown;
        gconfMode3.set(QVariant(GCONF_DISABLED));
    }

    connect( &radio, SIGNAL(modeChanged(int)),          this, SLOT(modeChanged(int)) );
    connect( &radio, SIGNAL(modeChangeFailed(QString)), this, SLOT(modeChangeFailed(QString)) );

}

void Switcher::changeMode()
{
    if(radio.mode() == mode1)
        radio.setMode(mode2);
    else if(radio.mode() == mode2 && mode3 == Cellular::RadioAccess::Unknown)
        radio.setMode(mode1);
    else if (radio.mode() == mode2)
        radio.setMode(mode3);
    else if (radio.mode() == mode3)
        radio.setMode(mode1);
    else
        QCoreApplication::instance()->quit();
}

void Switcher::modeChanged(int mode)
{
    if (dynIcon) {
        IconSwitcher iconswitcher;

        switch (mode) {
        case Cellular::RadioAccess::DualMode:
            iconswitcher.setIconDual();
            break;
        case Cellular::RadioAccess::OnlyThreeG:
            iconswitcher.setIcon3G();
            break;
        case Cellular::RadioAccess::OnlyTwoG:
            iconswitcher.setIcon2G();
            break;
        }
    }

    if(notifications) {
        MNotification notification(MNotification::DeviceEvent);

        switch (mode) {
        case Cellular::RadioAccess::DualMode:
            notification.setImage("icon-m-common-3g");
            notification.setBody("Dual mode");
            break;
        case Cellular::RadioAccess::OnlyThreeG:
            notification.setImage("icon-m-common-3g");
            notification.setBody("Only 3G");
            break;
        case Cellular::RadioAccess::OnlyTwoG:
            notification.setImage("icon-m-common-gsm");
            notification.setBody("Only 2G");
            break;
        }
        notification.publish();
    }
    QCoreApplication::instance()->quit();
}


void Switcher::modeChangeFailed(QString error)
{
    MNotification notification(MNotification::DeviceEvent);

    notification.setImage("icon-l-error");
    notification.setBody(error);
    notification.publish();

    QCoreApplication::instance()->quit();
}
