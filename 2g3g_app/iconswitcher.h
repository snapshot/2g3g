#ifndef ICONSWITCHER_H
#define ICONSWITCHER_H

#include <QObject>
#include <QFile>

class IconSwitcher
{
public:
    explicit IconSwitcher();
    void setIcon2G();
    void setIcon3G();
    void setIconDual();
    void createIcon();

private:
    QFile desktop;
    QFile newdesktop;
    void openFile();
    bool readFile(QStringList &lines);
    bool writeFile(const QStringList &lines);
};

#endif // ICONSWITCHER_H
