
#INCLUDEPATH += ../common
#HEADERS += gconfvalues.h

DEFINES += \
    GCONF_DISABLED=0 \
    GCONF_2G=1 \
    GCONF_3G=2 \
    GCONF_DUAL=3 \
    MODE1_DEFAULT=1 \
    MODE2_DEFAULR=3 \
    MODE3_DEFAULT=0 \
    MODE1_GCONFKEY=\\\"\"/apps/2g3g/mode1\"\\\" \
    MODE2_GCONFKEY=\\\"\"/apps/2g3g/mode2\"\\\" \
    MODE3_GCONFKEY=\\\"\"/apps/2g3g/mode3\"\\\" \
    DYNICON_GCONFKEY=\\\"\"/apps/2g3g/dynicon\"\\\" \
    NOTIF_GCONFKEY=\\\"\"/apps/2g3g/notifications\"\\\" \
