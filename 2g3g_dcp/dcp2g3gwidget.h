#ifndef DCP2G3GWIDGET_H
#define DCP2G3GWIDGET_H

#include <DcpStylableWidget>

class QGraphicsLinearLayout;
class MButtonGroup;
class Dcp2g3gConf;

struct Mode {
    Mode(int m1, int m2, int m3) :
        mode1(m1),
        mode2(m2),
        mode3(m3)
    {
    }

    int mode1;
    int mode2;
    int mode3;
};

class Dcp2G3GWidget : public DcpStylableWidget
{
    Q_OBJECT

public:
    Dcp2G3GWidget(QGraphicsWidget *parent = 0);
    virtual ~Dcp2G3GWidget();

signals:

private:
    void initWidget();
    void addTitle();
    void addCombo();
    void addDynIconButton();
    void addNotificationsButton();
    void addCreateIconButton();

    QStringList modeNames;
    QList<Mode> modeValues;

    QGraphicsLinearLayout *mainLayout;
    MButtonGroup* mode1ButtonGroup;
    MButtonGroup* mode2ButtonGroup;
    MButtonGroup* mode3ButtonGroup;
    Dcp2g3gConf *conf;


private slots:
    void comboboxIndexChanged(int id);
    void createIconButtonClicked(bool toggled);

};

#endif // DCP2G3GWIDGET_H

