#include "dcp2g3gapplet.h"
#include "dcp2g3gwidget.h"
#include <MAction>
#include <MLibrary>
#include <DcpStylableWidget>

M_LIBRARY
Q_EXPORT_PLUGIN2(dcp2g3gapplet, Dcp2G3GApplet)

DcpStylableWidget* Dcp2G3GApplet::constructStylableWidget(int widgetId)
{
    Q_UNUSED(widgetId)
    if (currentWidget == NULL)
        currentWidget = new Dcp2G3GWidget();
    return currentWidget;
}

QVector<MAction*> Dcp2G3GApplet::viewMenuItems()
{
    return QVector<MAction*>();
}
