#ifndef DCP2G3GAPPLET_H
#define DCP2G3GAPPLET_H

#include <DcpAppletIf>
#include <QObject>

class DcpStylableWidget;
class Dcp2G3GWidget;
class MAction;

class Dcp2G3GApplet : public QObject, public DcpAppletIf
{
	Q_OBJECT
	Q_INTERFACES(DcpAppletIf)

public:
    virtual DcpStylableWidget* constructStylableWidget(int widgetId);
    virtual QVector<MAction *> viewMenuItems();

protected:
    QPointer<Dcp2G3GWidget> currentWidget;
};

#endif // DCP2G3GAPPLET_H

