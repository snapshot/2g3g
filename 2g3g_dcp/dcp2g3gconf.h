#ifndef SCP2G3GCONF_H
#define SCP2G3GCONF_H

#include <QObject>

class Dcp2g3gConf : public QObject
{
    Q_OBJECT
public:
    explicit Dcp2g3gConf(QObject *parent = 0);
    
public slots:
    void setMode1(int mode1);
    void setMode2(int mode2);
    void setMode3(int mode3);
    void setDynamicIcon(bool dynicon);
    void setNotifications(bool notifications);

    int getMode1();
    int getMode2();
    int getMode3();
    bool getDynamicIcon();
    bool getNotifications();
    
private:

};

#endif // SCP2G3GCONF_H
