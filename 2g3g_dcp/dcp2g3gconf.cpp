#include "dcp2g3gconf.h"
#include <gconfitem.h>

namespace {
    static const QString mode1GConfKey ("/apps/2g3g/mode1");
    static const QString mode2GConfKey ("/apps/2g3g/mode2");
    static const QString mode3GConfKey ("/apps/2g3g/mode3");
    static const QString dyniconGConfKey ("/apps/2g3g/dynicon");
    static const QString notificationsGConfKey ("/apps/2g3g/notifications");
}

Dcp2g3gConf::Dcp2g3gConf(QObject *parent) :
    QObject(parent)
{
}

void Dcp2g3gConf::setMode1(int mode1)
{
    GConfItem gconfMode1(mode1GConfKey);
    gconfMode1.set(mode1);
}

void Dcp2g3gConf::setMode2(int mode2)
{
    GConfItem gconfMode2(mode2GConfKey);
    gconfMode2.set(mode2);
}

void Dcp2g3gConf::setMode3(int mode3)
{
    GConfItem gconfMode3(mode3GConfKey);
    gconfMode3.set(mode3);
}

int Dcp2g3gConf::getMode1()
{
    GConfItem gconfMode1(mode1GConfKey);
    return gconfMode1.value().toInt();
}

int Dcp2g3gConf::getMode2()
{
    GConfItem gconfMode2(mode2GConfKey);
    return gconfMode2.value().toInt();
}

int Dcp2g3gConf::getMode3()
{
    GConfItem gconfMode3(mode3GConfKey);
    return gconfMode3.value().toInt();
}



void Dcp2g3gConf::setDynamicIcon(bool dynicon)
{
    GConfItem gconfDynIcon(dyniconGConfKey);
    gconfDynIcon.set(dynicon);
}

bool Dcp2g3gConf::getDynamicIcon()
{
    GConfItem gconfDynIcon(dyniconGConfKey);
    return gconfDynIcon.value().toBool();
}


void Dcp2g3gConf::setNotifications(bool notifications)
{
    GConfItem gconfNotifications(notificationsGConfKey);
    gconfNotifications.set(notifications);
}

bool Dcp2g3gConf::getNotifications()
{
    GConfItem gconfNotifications(notificationsGConfKey);
    return gconfNotifications.value().toBool();
}
