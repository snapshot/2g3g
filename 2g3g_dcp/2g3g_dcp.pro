DCP_PREFIX = /usr
DCP_DESKTOP_DIR = $$DCP_PREFIX/share/duicontrolpanel/desktops
DCP_APPLET_DIR = $$DCP_PREFIX/lib/duicontrolpanel/applets

TEMPLATE      = lib
CONFIG       += plugin gui meegotouchcore duicontrolpanel link_pkgconfig
PKGCONFIG    += gq-gconf

MOC_DIR	      = .moc
OBJECTS_DIR   = .objects

HEADERS       += \
    dcp2g3gwidget.h \
    dcp2g3gapplet.h \
    gconfvalues.h \
    dcp2g3gconf.h

SOURCES       = \
    dcp2g3gwidget.cpp \
    dcp2g3gapplet.cpp \
    dcp2g3gconf.cpp

include(../common/common.pri)

DESTDIR       = ../lib
TARGET        = $$qtLibraryTarget(dcp2g3gapplet)
desktop.files += 2g3g_dui.desktop
desktop.path = $$DCP_DESKTOP_DIR
target.path += $$DCP_APPLET_DIR

INSTALLS += target desktop
