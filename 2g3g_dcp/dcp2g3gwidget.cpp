#include "dcp2g3gwidget.h"
#include "dcp2g3gconf.h"
#include <MWidgetCreator>
#include <QGraphicsLinearLayout>
#include <MLabel>
#include <MButton>
#include <MButtonGroup>
#include <MSeparator>
#include <MComboBox>
#include <QProcess>


M_REGISTER_WIDGET_NO_CREATE (Dcp2G3GWidget)

Dcp2G3GWidget::Dcp2G3GWidget (QGraphicsWidget *parent) :
    DcpStylableWidget(parent)
{
    conf = new Dcp2g3gConf(this);

    modeNames << "2G  -  3G"
              << "2G  -  Dual"
              << "3G  -  Dual"
              << "2G  -  3G  -  Dual"
              << "3G  -  2G  -  Dual";

    modeValues << Mode(GCONF_2G, GCONF_3G,   GCONF_DISABLED)
               << Mode(GCONF_2G, GCONF_DUAL, GCONF_DISABLED)
               << Mode(GCONF_3G, GCONF_DUAL, GCONF_DISABLED)
               << Mode(GCONF_2G, GCONF_3G,   GCONF_DUAL)
               << Mode(GCONF_3G, GCONF_2G,   GCONF_DUAL);

    initWidget();
}

void Dcp2G3GWidget::initWidget() {
    mainLayout = new QGraphicsLinearLayout(Qt::Vertical);
    mainLayout->setContentsMargins (0, 0, 0, 0);
    mainLayout->setSpacing (25);

    addTitle();
    addCombo();
    addNotificationsButton();
    addDynIconButton();
//    addCreateIconButton();

    mainLayout->addStretch();

    setLayout(mainLayout);
}

void Dcp2G3GWidget::addTitle() {
    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout (Qt::Vertical);
    layout->setContentsMargins (0, 0, 0, 0);
    layout->setSpacing (0);

    MLabel *title = new MLabel("2G3G");
    title->setStyleName("CommonHeaderInverted");
    layout->addItem(title);

    MSeparator *stretcher = new MSeparator();
    stretcher->setStyleName ("CommonHeaderDividerInverted");
    layout->addItem (stretcher);

    mainLayout->addItem(layout);
    mainLayout->setStretchFactor(layout, 0);
}


void Dcp2G3GWidget::addCombo()
{
    MComboBox *modeComboBox = new MComboBox();
    modeComboBox->setTitle("Mode");
    modeComboBox->setStyleName ("CommonComboBoxInverted");

    modeComboBox->addItems(modeNames);

    modeComboBox->setCurrentIndex(-1);
    int current_mode1 = conf->getMode1();
    int current_mode2 = conf->getMode2();
    int current_mode3 = conf->getMode3();
    for (int i = 0; i < modeValues.size(); ++i) {
        if (modeValues[i].mode1 == current_mode1
                && modeValues[i].mode2 == current_mode2
                && modeValues[i].mode3 == current_mode3 ) {
            modeComboBox->setCurrentIndex(i);
            break;
        }
    }

    connect(modeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboboxIndexChanged(int)));

    mainLayout->addItem(modeComboBox);
}

void Dcp2G3GWidget::addDynIconButton()
{
        QGraphicsLinearLayout *layout = new QGraphicsLinearLayout (Qt::Horizontal);

        MLabel *label = new MLabel;
        label->setWordWrap (true);
        label->setStyleName ("CommonSingleTitleInverted");
        label->setText ("Dynamic icon");
        layout->addItem (label);
        layout->setAlignment (label, Qt::AlignVCenter);

        MButton *button = new MButton;
        button->setCheckable (true);
        button->setViewType (MButton::switchType);
        button->setStyleName ("CommonRightSwitchInverted");
        button->setChecked (conf->getDynamicIcon());

        connect (button, SIGNAL (toggled (bool)),
                 conf, SLOT(setDynamicIcon(bool)));
        connect (button, SIGNAL(toggled(bool)),
                 this, SLOT(createIconButtonClicked(bool)));


        layout->addItem (button);
        layout->setAlignment (button, Qt::AlignVCenter | Qt::AlignRight);

        mainLayout->addItem (layout);
        mainLayout->setStretchFactor(layout, 0);
}

void Dcp2G3GWidget::addNotificationsButton()
{
        QGraphicsLinearLayout *layout = new QGraphicsLinearLayout (Qt::Horizontal);

        MLabel *label = new MLabel;
        label->setWordWrap (true);
        label->setStyleName ("CommonSingleTitleInverted");
        label->setText ("Notifications");
        layout->addItem (label);
        layout->setAlignment (label, Qt::AlignVCenter);

        MButton *button = new MButton;
        button->setCheckable (true);
        button->setViewType (MButton::switchType);
        button->setStyleName ("CommonRightSwitchInverted");
        button->setChecked (conf->getNotifications());

        connect (button, SIGNAL (toggled (bool)),
                 conf, SLOT(setNotifications(bool)));

        layout->addItem (button);
        layout->setAlignment (button, Qt::AlignVCenter | Qt::AlignRight);

        mainLayout->addItem (layout);
        mainLayout->setStretchFactor(layout, 0);
}

void Dcp2G3GWidget::addCreateIconButton()
{
        MButton *button = new MButton;
        button->setText("Create icon");
        button->setStyleName ("CommonSingleButtonInverted");

        connect (button, SIGNAL(clicked()),
                 this, SLOT(createIconButtonClicked(bool)));

        mainLayout->addItem(button);
        mainLayout->setAlignment (button, Qt::AlignCenter);
}

void Dcp2G3GWidget::comboboxIndexChanged(int id)
{
    conf->setMode1( modeValues[id].mode1 );
    conf->setMode2( modeValues[id].mode2 );
    conf->setMode3( modeValues[id].mode3 );
}

void Dcp2G3GWidget::createIconButtonClicked(bool toggled=true)
{
    if(toggled)
        return;

    QProcess process;
    QStringList args;
    args << "-u" << "user"
         << "/opt/2g3g_app/bin/2g3g_app" << "-f";
    process.start("/usr/bin/aegis-exec", args);
    process.waitForFinished();
}



Dcp2G3GWidget::~Dcp2G3GWidget()
{
}
